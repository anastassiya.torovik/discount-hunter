import asyncio
from fastapi.testclient import TestClient
from unittest.mock import AsyncMock
import pytest_asyncio

from api.app.main import app


@pytest_asyncio.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture(scope="session")
async def test_client():
    with TestClient(app) as client:
        yield client


@pytest_asyncio.fixture(scope="function")
def mock_search(mocker):
    es_search_mock = AsyncMock()
    mocker.patch("api.app.main.AsyncElasticsearch.search", new=es_search_mock)
    yield es_search_mock
