import api.app.functions as funcs
from api.app.api_objects import SortParams, TextQuery, PaginationParams, DiscountedItem, ApiResponse
from pydantic import ValidationError
import pytest
from typing import List
from api.app.functions import group_hits_by_server


def test_parse_sort_parameter_single_field():
    sort_params = SortParams(sort=["price"])
    result = funcs.parse_sort_parameter(sort_params)
    assert result == [{"price": {"order": "asc"}}]


def test_parse_sort_parameter_single_field_desc():
    sort_params = SortParams(sort=["-price"])
    result = funcs.parse_sort_parameter(sort_params)
    assert result == [{"price": {"order": "desc"}}]


def test_parse_sort_parameter_multiple_fields():
    sort_params = SortParams(sort=["-price", "server"])
    result = funcs.parse_sort_parameter(sort_params)
    assert result == [{"price": {"order": "desc"}}, {"server": {"order": "asc"}}]


def test_parse_sort_parameter_empty():
    sort_params = SortParams(sort=[])
    result = funcs.parse_sort_parameter(sort_params)
    assert result == []


def test_parse_sort_parameter_invalid_field():
    with pytest.raises(ValidationError):
        funcs.parse_sort_parameter(SortParams(sort=["invalid_field"]))


def test_build_query_no_text_query():
    query = funcs.build_query(TextQuery(text_query=[]), PaginationParams(offset=0, size=10), SortParams(sort=[]))
    assert query["from"] == 0
    assert query["size"] == 10
    assert "multi_match" not in query["query"]
    assert "match_all" in query["query"]


def test_build_query_with_text_query():
    text_query = TextQuery(text_query=["apple", "banana"])
    query = funcs.build_query(text_query, PaginationParams(offset=10, size=20), SortParams(sort=[]))
    assert query["from"] == 10
    assert query["size"] == 20
    assert "multi_match" in query["query"]
    assert "match_all" not in query["query"]
    assert query["query"]["multi_match"]["query"] == "apple banana"


def test_build_query_with_sort():
    text_query = TextQuery(text_query=["apple", "banana"])
    query = funcs.build_query(text_query, PaginationParams(offset=0, size=10), SortParams(sort=["price"]))
    assert query["from"] == 0
    assert query["size"] == 10
    assert "multi_match" in query["query"]
    assert "match_all" not in query["query"]
    assert query["sort"] == [{"price": {"order": "asc"}}]


@pytest.fixture
def sample_discounted_items() -> List[DiscountedItem]:
    items = [
        {
            "id": 1,
            "name": "Item 1",
            "slug": "item-1",
            "images": [],
            "price": 10.0,
            "original_price": 15.0,
            "unit": "ks",
            "percentage_discount": 33,
            "server": "kosik",
            "product_quantity": None,
            "price_per_unit": 10.0,
            "main_category": {"id": 1, "name": "Category 1"},
            "currency": "CZK",
            "country_code": "CZE",
            "brand": None,
            "discount_valid_till": "2023-04-24T23:59:00+02:00",
        },
        {
            "id": 2,
            "name": "Item 2",
            "slug": "item-2",
            "images": [],
            "price": 20.0,
            "original_price": 30.0,
            "unit": "ks",
            "percentage_discount": 33,
            "server": "rohlik",
            "product_quantity": None,
            "price_per_unit": 20.0,
            "main_category": {"id": 2, "name": "Category 2"},
            "currency": "CZK",
            "country_code": "CZE",
            "brand": None,
            "discount_valid_till": "2023-04-24T23:59:00+02:00",
        },
    ]
    return [DiscountedItem.parse_obj(item) for item in items]


def test_group_hits_by_server(sample_discounted_items):
    result = group_hits_by_server(sample_discounted_items)

    assert isinstance(result, ApiResponse)
    assert len(result.kosik.hits) == 1
    assert len(result.rohlik.hits) == 1
    assert result.kosik.total_hits == 1
    assert result.rohlik.total_hits == 1

    kosik_item = result.kosik.hits[0]
    assert kosik_item.id == 1
    assert kosik_item.name == "Item 1"

    rohlik_item = result.rohlik.hits[0]
    assert rohlik_item.id == 2
    assert rohlik_item.name == "Item 2"


def test_group_hits_by_server_no_hits():
    result = group_hits_by_server([])

    assert isinstance(result, ApiResponse)
    assert len(result.kosik.hits) == 0
    assert len(result.rohlik.hits) == 0
    assert result.kosik.total_hits == 0
    assert result.rohlik.total_hits == 0


