mocked_es_response = {
    "hits": {
        "hits": [
            {
                "_index": "discounts",
                "_id": "kosik-42237",
                "_score": 1.0,
                "_source": {
                    "original_price": 44.9,
                    "server": "kosik",
                    "images": [
                        "https://static-new.kosik.cz/k3wCdnContainerk3w-static-cz-prod/images/thumbs/1u/WIDTHxHEIGHTx1_1ujn8nl0eyv5.jpg"
                    ],
                    "discount_valid_till": "2023-04-18T23:59:00+02:00",
                    "product_quantity": {
                        "unit": "ks",
                        "value": 20.0
                    },
                    "meta_updated_timestamp": "2023-04-16T20:36:34.347980051Z",
                    "country_code": None,
                    "unit": "ks",
                    "price": 33.9,
                    "name": "Teekanne Forest Fruits World of Fruits 50g",
                    "percentage_discount": 24,
                    "main_category": {
                        "name": "Nápoje",
                        "id": 1107
                    },
                    "currency": "CZK",
                    "id": 42237,
                    "price_per_unit": 1.6949999999999998,
                    "brand": None,
                    "slug": "teekanne-forest-fruits-world-of-fruits-50g"
                }
            }
        ]
    }
}

empty_es_response = {
    "hits": {
        "hits": []
    }
}

expected_resp = [{'id': 42237, 'name': 'Teekanne Forest Fruits World of Fruits 50g',
                  'slug': 'teekanne-forest-fruits-world-of-fruits-50g',
                  'images': ['https://static-new.kosik.cz/k3wCdnContainerk3w-static-cz-prod/images/thumbs/1u/WIDTHxHEIGHTx1_1ujn8nl0eyv5.jpg'],
                  'price': 33.9, 'original_price': 44.9, 'unit': 'ks', 'percentage_discount': 24, 'server': 'kosik',
                  'product_quantity': {'value': 20.0, 'unit': 'ks'}, 'price_per_unit': 1.6949999999999998,
                  'main_category': {'id': 1107, 'name': 'Nápoje'}, 'currency': 'CZK', 'country_code': None,
                  'brand': None, 'discount_valid_till': '2023-04-18T23:59:00+02:00'}]