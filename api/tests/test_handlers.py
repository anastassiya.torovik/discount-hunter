import pytest
from .storage import mocked_es_response, empty_es_response, expected_resp


@pytest.mark.asyncio
async def test_ping(test_client):
    res = test_client.get(f"/ping")
    resp = res.json()
    assert res.status_code == 200
    assert resp["Success"]


@pytest.mark.asyncio
async def test_hello(test_client):
    res = test_client.get(f"/")
    assert res.status_code == 200


@pytest.mark.asyncio
async def test_get_discounts(test_client, mock_search):
    mock_search.return_value = mocked_es_response
    res = test_client.get(f"/discounts")
    resp = res.json()
    assert res.status_code == 200
    assert resp == expected_resp


@pytest.mark.asyncio
async def test_get_discounts_invalid_pagination(test_client):
    res = test_client.get(f"/discounts", params={"offset": 9999, "size": 100})
    resp = res.json()
    assert res.status_code == 400
    assert "detail" in resp
    assert "Invalid pagination parameters" in resp["detail"]


@pytest.mark.asyncio
async def test_get_discounts_no_results(test_client, mock_search):
    mock_search.return_value = empty_es_response
    res = test_client.get(f"/discounts?offset=1000&size=10")
    resp = res.json()
    assert res.status_code == 200
    assert resp == []
