from .api_objects import PaginationParams, SortParams, TextQuery, DiscountedItem, ApiResponse
from typing import List, Dict


def parse_sort_parameter(sort_params: SortParams) -> List[Dict[str, Dict[str, str]]]:
    sort_config = []

    for field in sort_params.sort:
        if field.startswith('-'):
            field_name = field[1:]
            order = "desc"
        else:
            field_name = field
            order = "asc"

        sort_config.append({field_name: {"order": order}})

    return sort_config


def build_query(query: TextQuery, pagination: PaginationParams, sort: SortParams):
    sort_config = parse_sort_parameter(sort)
    if query.text_query:
        query = {
            "from": pagination.offset,
            "size": pagination.size,
            "query": {
                "multi_match": {
                    "query": " ".join(query.text_query),
                    "fields": ["slug"],
                    "type": "best_fields"
                }
            },
            "sort": sort_config
        }
    else:
        query = {
            "from": pagination.offset,
            "size": pagination.size,
            "query": {
                "match_all": {}
            },
            "sort": sort_config
        }
    return query


def group_hits_by_server(data: List[DiscountedItem]) -> ApiResponse:
    data = [item.dict() for item in data]

    grouped_data = {}

    for item in data:
        server = item["server"]

        if server not in grouped_data:
            grouped_data[server] = {"total_hits": 0, "hits": []}

        grouped_data[server]["hits"].append(item)
        grouped_data[server]["total_hits"] += 1
    return ApiResponse.parse_obj(grouped_data)