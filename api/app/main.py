from fastapi import FastAPI
from fastapi.routing import APIRouter, APIRoute, Request, JSONResponse
from elasticsearch import AsyncElasticsearch
from .config import ELASTICSEARCH_HOST

from .handlers import ping, hello, api_get_discounts, api_health_check_elastic, api_get_discounts_by_server

# create instance of the app
app = FastAPI(title="discount-hunter")
elastic = AsyncElasticsearch(ELASTICSEARCH_HOST)
app.state.elastic = elastic


@app.exception_handler(ValueError)
async def value_error_handler(request: Request, exc: ValueError):
    return JSONResponse(
        status_code=400,
        content={"detail": str(exc)},
    )


routes = [
    APIRoute(path="/", endpoint=hello, methods=["GET"]),
    APIRoute(path="/ping", endpoint=ping, methods=["GET"]),
    APIRoute(path="/readiness", endpoint=api_health_check_elastic, methods=["GET"]),
    APIRoute(path="/discounts", endpoint=api_get_discounts, methods=["GET"]),
    APIRoute(path="/discounts/by_server", endpoint=api_get_discounts_by_server, methods=["GET"]),
]

app.include_router(APIRouter(routes=routes))
