from elasticsearch import AsyncElasticsearch
from starlette.requests import Request
from typing import List
from .api_objects import DiscountedItem, PaginationParams, SortParams, TextQuery, ApiResponse
from .functions import build_query, group_hits_by_server
from fastapi import Depends


async def ping():
    return {"Success": True}


async def hello():
    return {"Message": "Hi there! Welcome to discount-hunter"}


async def api_health_check_elastic(request: Request):
    """Healthchecker for Elasticsearch index"""
    elastic: AsyncElasticsearch = request.app.state.elastic
    response = await elastic.cluster.health(index="discounts")
    return {"status": response["status"], "details": response}


async def api_get_discounts(request: Request, query: TextQuery = Depends(), pagination: PaginationParams = Depends(),
                            sort: SortParams = Depends()) -> \
        List[DiscountedItem]:
    """Fetch discounted items from elasticsearch index 'discounts' using offset and size parameters"""
    elastic: AsyncElasticsearch = request.app.state.elastic
    es_query = build_query(query, pagination, sort)
    response = await elastic.search(index="discounts", body=es_query)
    documents = [DiscountedItem.parse_obj(hit["_source"]) for hit in response["hits"]["hits"]]
    return documents


async def api_get_discounts_by_server(request: Request, query: TextQuery = Depends()) -> ApiResponse:
    response = await api_get_discounts(request, query, PaginationParams(offset=0, size=9999), SortParams(sort=[]))
    response = group_hits_by_server(response)
    return response

