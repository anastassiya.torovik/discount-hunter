from typing import Optional, List, Literal
from pydantic import BaseModel, validator, Field
from fastapi import Query


class ProductQuantity(BaseModel):
    value: float
    unit: Optional[str]


class MainCategory(BaseModel):
    id: int
    name: Optional[str]


class DiscountedItem(BaseModel):
    id: int
    name: str
    slug: str
    images: Optional[List[str]] = []
    price: float
    original_price: float
    unit: str
    percentage_discount: Optional[int]
    server: str
    product_quantity: Optional[ProductQuantity]
    price_per_unit: float
    main_category: Optional[MainCategory]
    currency: str = 'CZK'
    country_code: Optional[str]
    brand: Optional[str]
    discount_valid_till: Optional[str]

    class Config:
        extra = "ignore"


class PaginationParams(BaseModel):
    offset: Optional[int] = Field(Query(default=0, gte=0, le=10000))
    size: Optional[int] = Field(Query(default=10, gte=0, le=10000))

    @validator('size', 'offset')
    def check_sum(cls, value, values):
        offset = values.get('offset', 0)
        size = values.get('size', 10)

        if offset + size >= 10000:
            raise ValueError("Invalid pagination parameters: offset + size must be less than 10000.")

        return value


class Hits(BaseModel):
    total_hits: int = Field(0)
    hits: List[DiscountedItem] = Field([])


class ApiResponse(BaseModel):
    kosik: Hits = Field(Hits(), alias="kosik")
    rohlik: Hits = Field(Hits(), alias="rohlik")


class SortParams(BaseModel):
    sort: Optional[List[Literal['price', '-price', 'server', '-server', 'price_per_unit', '-price_per_unit', 'name',
    '-name']]] = Field(Query([]))


class TextQuery(BaseModel):
    text_query: Optional[List[str]] = Field(Query([]))
