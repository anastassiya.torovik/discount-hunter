#!/bin/sh

LOG_FILE="/var/log/cron.log"

echo "$(date) - Running delete-by-query" >> "${LOG_FILE}"
curl -k -X POST "http://es-container:9200/discounts/_delete_by_query?pretty" -H 'Content-Type: application/json' -d'
{
  "query": {
    "bool": {
      "filter": [
        {
          "range": {
            "meta_updated_timestamp": {
              "lt": "now-1d"
            }
          }
        }
      ]
    }
  }
}' >> "${LOG_FILE}" 2>&1

echo "$(date) - Completed delete-by-query" >> "${LOG_FILE}"
