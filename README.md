# discount-hunter

## How it works:

### [Work Flow Schema](documentation/sequence_diagram.md)

![Sequence Diagram](https://www.plantuml.com/plantuml/png/XP91Rzim38Nl_1L4xZvqnm4kkYKRCEoIjTS2XIonis1LyaIfGF_zA7bGR2HBJicZFhudqdhWf6BARmwKJY7Ytbdo2HMZhlzWbcEhxQoakljo_85F5D-jfbrXr6p_IJ0ggK4nuUwRKomjJ2hgiMAqhi2VJ5cmd-frKlWrXebYfQPIU1bgOHlBAKHwAGaLhF8WQajfHygVg2hFgLTAUZnHLLPg2DaR5J_aPe7jX3BhSgsYHAo0np2w5H55BWZqFHOpFCQWYTdwttYqCpdh2ViUO5agTlUhrU5dV2HP2YU0LRKJQBCuuNvbaloIS8OkpjKhvUZbX-VWcG2WpIYvRHmXlvTwTTiYRPuj0wrLcDOWpC6objqaNhZMVeFRXgw_lh0rg-EGewREjCOKPgkVVOduxp70nakysrCGuwkjp_vQBi39wFp6zOpCxTkqvtkjgK4Nty5JELssQ8PzySZpFWQF5zW3EKg49cZ6Uxn3epwGtiixklk4empAy43Uv3Vt5m00)


## How to run (native with Docker services)

- `docker compose up -d`

**Kibana**: http://127.0.0.1:5601/app/dev_tools#/console

**Elasticsearch**: http://127.0.0.1:9200

**API service**: http://127.0.0.1:8000

**API docs**: http://127.0.0.1:8000/docs

