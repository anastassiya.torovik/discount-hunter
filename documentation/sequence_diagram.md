```plantuml
@startuml
actor Client as client
participant "API Service" as apiService
database "Elasticsearch" as elasticsearch
queue "Kafka Broker" as kafka_broker
participant "store_data" as storeData
participant "clean_data" as cleanData
participant "fetch_data" as fetchData
boundary "Kosik API" as kosikAPI
boundary "Rohlik API" as rohlikAPI

== Data Processing Pipeline ==

client -> apiService : Request

apiService --> elasticsearch : Query Data

apiService --> client : Return Response


fetchData -> kosikAPI : Fetch Data
fetchData -> rohlikAPI : Fetch Data

kosikAPI -> fetchData : Discounts Data
rohlikAPI -> fetchData : Discounts Data

fetchData -> kafka_broker : kosik-source-discounts topic\nrohlik-source-discounts topic

kafka_broker -> cleanData : rohlik-source-discounts topic\nkosik-source-discounts topic

cleanData -> kafka_broker : cleaned-discounts topic

kafka_broker -> storeData : cleaned-discounts topic

storeData --> elasticsearch : Store Data
dbCleanupCron -> elasticsearch : Delete docs > 1 day every 12 hours
@enduml
```