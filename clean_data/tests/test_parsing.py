import pytest
from clean_data.app.objects_parsers import RohlikParser, KosikParser
from clean_data.app.api_objects import RohlikItem, KosikItem

# RohlikParser tests

@pytest.fixture
def rohlik_item():
    rohlik_item_dict = {
        "id": 1352645,
        "name": "Pekárna Kabát Perníková vajíčka",
        "slug": "pekarna-kabat-pernikova-vajicka",
        "images": ["https://cdn.rohlik.cz/images/grocery/products/1352645/1352645-1554371725.jpg"],
        "unit": "kg",
        "textualAmount": "150 g",
        "mainCategoryId": 300101032,
        "countries": [{"code": "CZE"}],
        "brand": "Pekárna Kabát",
        "server": "rohlik",
        "sales": [
            {
                "price": {"amount": 99.9},
                "pricePerUnit": {"amount": 666.0},
                "originalPrice": {"amount": 129.9},
                "validTill": "2023-04-29T23:59:00+02:00",
            }
        ],
        "currency": "CZK",
    }
    return RohlikItem(
        **rohlik_item_dict
    )


@pytest.mark
def test_rohlik_parser_transform_item_json(rohlik_item):
    parser = RohlikParser()
    transformed = parser.transform_item_json(rohlik_item)

    # Add assertions for the expected output
    assert transformed["id"] == 1352645
    assert transformed["name"] == 'Pekárna Kabát Perníková vajíčka'
    assert transformed["slug"] == 'pekarna-kabat-pernikova-vajicka'
    assert transformed["images"] == ['https://cdn.rohlik.cz/images/grocery/products/1352645/1352645-1554371725.jpg']
    assert transformed["price"] == 99.9
    assert transformed["original_price"] == 129.9
    assert transformed["unit"] == 'kg'
    assert transformed["percentage_discount"] == (129.9 - 99.9) / 129.9
    assert transformed["server"] == 'rohlik'
    assert transformed["product_quantity"] == {"value": 150, "unit": "g"}
    assert transformed["price_per_unit"] == 666.0
    assert transformed["main_category"] == {"id": 300101032}
    assert transformed["currency"] == 'CZK'
    assert transformed["country_code"] == 'CZE'
    assert transformed["brand"] == 'Pekárna Kabát'
    assert transformed["discount_valid_till"] == '2023-04-29T23:59:00+02:00'

# KosikParser tests

@pytest.fixture
def kosik_item():
    kosik_item_dict = {
        "id": 721704,
        "name": "Jar Lemon Prostředek na nádobí 2x750ml",
        "image": "https://static-new.kosik.cz/k3wCdnContainerk3w-static-cz-prod/images/thumbs/z6/WIDTHxHEIGHTx1_z64bnmom47sr.jpg",
        "url": "/produkt/jar-lemon-2x750ml",
        "price": 89.9,
        "unit": "ks",
        "recommendedPrice": 149.9,
        "percentageDiscount": 40,
        "productQuantity": {"value": 1.5, "unit": "l"},
        "mainCategory": {"id": 1455, "name": "Drogerie a kosmetika"},
        "pricePerUnit": {"price": 59.93333333333334, "unit": "l"},
        "countryCode": "CZE",
        "server": "kosik",
        "actionLabel": "Akce platí do 18. 4.",
    }
    return KosikItem(
        **kosik_item_dict
    )

@pytest.mark
def test_kosik_parser_transform_item_json(kosik_item):
    parser = KosikParser()
    transformed = parser.transform_item_json(kosik_item)

    # Add assertions for the expected output
    assert transformed["id"] == 721704
    assert transformed["name"] == 'Jar Lemon Prostředek na nádobí 2x750ml'
    assert transformed["images"] == ['https://static-new.kosik.cz/k3wCdnContainerk3w-static-cz-prod/images/thumbs/z6/WIDTHxHEIGHTx1_z64bnmom47sr.jpg']
    assert transformed["slug"] == 'jar-lemon-2x750ml'
    assert transformed["price"] == 89.9
    assert transformed["unit"] == 'ks'
    assert transformed["original_price"] == 149.9
    assert transformed["percentage_discount"] == 40
    assert transformed["product_quantity"] == {"value": 1.5, "unit": "l"}
    assert transformed["price_per_unit"] == 59.93333333333334
    assert transformed["main_category"] == {"id": 1455, "name": "Drogerie a kosmetika"}
    assert transformed["currency"] == 'CZK'
    assert transformed["server"] == 'kosik'
    assert transformed["country_code"] == 'CZE'
    assert transformed["discount_valid_till"] == '2023-04-29T23:59:00+02:00'
