from abc import ABC, abstractmethod
from aiokafka import ConsumerRecord
from api_objects import RohlikItem, DiscountItem, KosikItem
import logging
import sys
import re
from datetime import datetime, timezone, timedelta

logging.basicConfig(format='%(asctime)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    stream=sys.stdout)

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class Parser(ABC):

    @abstractmethod
    def validate_initial_item(self, item: ConsumerRecord):
        pass

    @abstractmethod
    def transform_item_json(self, item):
        pass

    @abstractmethod
    def validate_final_item(self, item: dict):
        pass

    def parse_item_in_flow(self, item: ConsumerRecord) -> dict:
        """workflow for parsing discount item"""
        item = self.validate_initial_item(item)
        item = self.transform_item_json(item)
        item = self.validate_final_item(item)
        return item.dict()


class RohlikParser(Parser):

    def validate_initial_item(self, item: ConsumerRecord) -> RohlikItem:
        item = RohlikItem(**item.value)
        return item

    def transform_item_json(self, item: RohlikItem) -> dict:
        price = item.sales[0].price.amount
        original_price = item.sales[0].originalPrice.amount
        product_quantity = None
        if item.textualAmount:
            pattern = re.compile(r"(?P<value>\d+)\s*(?P<unit>[a-zA-Z]+)")
            match = pattern.search(item.textualAmount)

            product_quantity = {
                "value": int(match.group("value")),
                "unit": match.group("unit").strip()
            }
        item = {
            "id": item.id,
            "name": item.name,
            "slug": item.slug,
            "images": item.images,
            "price": price,
            "original_price": original_price,
            "unit": item.unit,
            "percentage_discount": (original_price - price)/original_price,
            "server": item.server,
            "product_quantity": product_quantity,
            "price_per_unit": item.sales[0].pricePerUnit.amount,
            "main_category": {"id": item.mainCategoryId},
            "currency": item.currency,
            "country_code": item.countries[0].code if item.countries else None,
            "brand": item.brand,
            "discount_valid_till": datetime.fromisoformat(item.sales[0].validTill).isoformat()
            if item.sales[0].validTill else None
        }
        return item

    def validate_final_item(self, item: dict) -> DiscountItem:
        item = DiscountItem(**item)
        return item


class KosikParser(Parser):

    def validate_initial_item(self, item: ConsumerRecord) -> KosikItem:
        item = KosikItem(**item.value)
        return item

    def _parse_action_label(self, action_label: str) -> str:
        current_year = datetime.now().year
        day, month = map(int, re.findall(r"\d+", action_label))
        tz_offset = timedelta(hours=2)
        dt = datetime(current_year, month, day, 23, 59, 0, tzinfo=timezone(tz_offset))
        return dt.isoformat()

    def transform_item_json(self, item: KosikItem) -> dict:
        item = {
            "id": item.id,
            "name": item.name,
            "images": [item.image],
            "slug": item.url.split('/')[-1],
            "price": item.price,
            "unit": item.unit,
            "original_price": item.recommendedPrice,
            "percentage_discount": item.percentageDiscount,
            "product_quantity": item.productQuantity,
            "price_per_unit": item.pricePerUnit.price,
            "main_category": {
                "id": item.mainCategory.id,
                "name": item.mainCategory.name
            },
            "currency": "CZK",
            "server": "kosik",
            "country_code": item.countryCode,
            "discount_valid_till": self._parse_action_label(item.actionLabel) if item.actionLabel else None
        }
        return item

    def validate_final_item(self, item: dict) -> DiscountItem:
        item = DiscountItem(**item)
        return item



