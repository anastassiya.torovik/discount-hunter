import config
import logging
import sys
import asyncio
from aiokafka import AIOKafkaConsumer, AIOKafkaProducer
import json
from pydantic import ValidationError
from objects_parsers import RohlikParser, KosikParser, Parser

logging.basicConfig(format='%(asctime)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    stream=sys.stdout)

logger = logging.getLogger()
logger.setLevel(logging.INFO)


async def init_consumer(topic: str) -> AIOKafkaConsumer:
    consumer = AIOKafkaConsumer(
        topic,
        bootstrap_servers=[config.BOOTSTRAP_SERVER],
        auto_offset_reset="earliest",
        enable_auto_commit=True,
        value_deserializer=lambda x: json.loads(x.decode("utf-8")),
        key_deserializer=lambda x: x.decode("utf-8"),
    )
    return consumer


async def init_producer() -> AIOKafkaProducer:
    producer = AIOKafkaProducer(
        bootstrap_servers=[config.BOOTSTRAP_SERVER],
        value_serializer=lambda x: json.dumps(x).encode("utf-8"),
        key_serializer=lambda x: x.encode("utf-8"),
    )
    return producer


class CleanerApp:
    def __init__(self, producer: AIOKafkaProducer, consumer: AIOKafkaConsumer, parser: Parser):
        self.producer = producer
        self.consumer = consumer
        self.parser = parser

    async def produce(self, topic, key, value):
        await self.producer.send_and_wait(topic, key=key, value=value)
        logging.debug(f"Produced msg with key: {key}")

    async def run_app(self):
        await self.consumer.start()
        try:
            async for msg in self.consumer:
                logging.debug(f"Consumed msg with key: {msg.key}")
                try:
                    cleaned_item = self.parser.parse_item_in_flow(msg)
                    await self.produce(config.PRODUCER_TOPIC, msg.key, cleaned_item)
                except ValidationError as e:
                    logging.error(f"Validation error for message {msg.value}: error {e}")
        finally:
            await self.consumer.stop()


async def main():
    producer = await init_producer()

    rohlik_consumer = await init_consumer(config.ROHLIK_TOPIC)
    rohlik_parser = RohlikParser()
    rohlik_cleaner_app = CleanerApp(producer, rohlik_consumer, rohlik_parser)

    kosik_consumer = await init_consumer(config.KOSIK_TOPIC)
    kosik_parser = KosikParser()
    kosik_cleaner_app = CleanerApp(producer, kosik_consumer, kosik_parser)

    await producer.start()
    try:
        rohlik_task = asyncio.create_task(rohlik_cleaner_app.run_app())
        kosik_task = asyncio.create_task(kosik_cleaner_app.run_app())
        await asyncio.gather(kosik_task, rohlik_task)
    finally:
        await producer.stop()

if __name__ == "__main__":
    asyncio.run(main())
