from typing import Optional, List
from pydantic import BaseModel, validator
from datetime import datetime


class ProductQuantity(BaseModel):
    value: float
    unit: Optional[str]


class MainCategory(BaseModel):
    id: int
    name: Optional[str]

    class Config:
        extra = "ignore"


class PricePerUnit(BaseModel):
    price: float
    unit: str


class KosikItem(BaseModel):
    id: int
    name: str
    image: str
    url: str
    price: float
    unit: str
    recommendedPrice: float
    percentageDiscount: int
    productQuantity: Optional[ProductQuantity]
    mainCategory: Optional[MainCategory]
    pricePerUnit: PricePerUnit
    countryCode: Optional[str]
    server: str = 'kosik'
    actionLabel: Optional[str]

    class Config:
        """Ignore extra arguments passed to the request body"""
        extra = "ignore"


class Country(BaseModel):
    code: str

    class Config:
        extra = "ignore"


class Price(BaseModel):
    amount: float

    class Config:
        extra = "ignore"


class Sale(BaseModel):
    price: Price
    pricePerUnit: Price
    originalPrice: Price
    validTill: Optional[str]

    class Config:
        extra = "ignore"


class RohlikItem(BaseModel):
    id: int
    name: str
    slug: str
    images: Optional[List[str]]
    unit: str
    textualAmount: Optional[str]
    mainCategoryId: Optional[int]
    countries: Optional[List[Country]]
    brand: Optional[str]
    server: str = 'rohlik'
    sales: List[Sale]
    currency: str = 'CZK'

    @validator('sales', each_item=False)
    def sales_not_empty(cls, sales: List[Sale]) -> List[Sale]:
        if len(sales) == 0:
            raise ValueError("sales list should not be empty")
        return sales

    class Config:
        extra = "ignore"


class DiscountItem(BaseModel):
    id: int
    name: str
    slug: str
    images: Optional[List[str]] = []
    price: float
    original_price: float
    unit: str
    percentage_discount: Optional[int]
    server: str
    product_quantity: Optional[ProductQuantity]
    price_per_unit: float
    main_category: Optional[MainCategory]
    currency: str = 'CZK'
    country_code: Optional[str]
    brand: Optional[str]
    discount_valid_till: Optional[str]

    class Config:
        """Ignore extra arguments passed to the request body"""
        extra = "ignore"