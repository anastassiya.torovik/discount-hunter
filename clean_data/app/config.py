import os

KOSIK_TOPIC = os.getenv('KOSIK_TOPIC', 'kosik-source-discounts')
ROHLIK_TOPIC = os.getenv('ROHLIK_TOPIC', 'rohlik-source-discounts')
PRODUCER_TOPIC = os.getenv('PRODUCER_TOPIC', 'cleaned-discounts')
BOOTSTRAP_SERVER = os.getenv('BOOTSTRAP_SERVER', 'localhost:29092')