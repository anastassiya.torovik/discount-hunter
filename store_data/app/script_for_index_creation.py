from elasticsearch import AsyncElasticsearch, BadRequestError
import logging
import sys
import config

logging.basicConfig(format='%(asctime)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    stream=sys.stdout)

logger = logging.getLogger()
logger.setLevel(logging.INFO)

INDEX_NAME = 'discounts'

MAPPING_FOR_INDEX = {
    "properties": {
        "id": {"type": "keyword"},
        "name": {"type": "keyword"},
        "slug": {"type": "text"},
        "images": {"type": "keyword", "index": "false"},
        "price": {"type": "float"},
        "original_price": {"type": "float"},
        "unit": {"type": "keyword"},
        "percentage_discount": {"type": "float"},
        "server": {"type": "keyword"},
        "product_quantity": {
            "properties": {
                "value": {"type": "float"},
                "unit": {"type": "keyword"},
            }
        },
        "price_per_unit": {"type": "float"},
        "main_category": {
            "properties": {
                "id": {"type": "integer"},
                "name": {"type": "text"},
            }
        },
        "currency": {"type": "keyword"},
        "country_code": {"type": "keyword"},
        "brand": {"type": "text"},
        "discount_valid_till": {"type": "date"},
    }
}

PIPELINE = {
    "description": "timestamp inject pipeline",
    "processors": [{
        "set": {
            "field": "_source.meta_updated_timestamp",
            "value": "{{_ingest.timestamp}}"
        }
    },
    ]
}


async def create_index():
    elastic_client: AsyncElasticsearch = AsyncElasticsearch(hosts=config.ELASTICSEARCH_HOST)
    pipeline_id = "timestamp_inject_pipeline"
    await elastic_client.ingest.put_pipeline(id=pipeline_id, body=PIPELINE)
    logging.info(f'Created pipeline "{pipeline_id}"')

    try:
        response = await elastic_client.indices.create(
            index=INDEX_NAME,
            body={
                "mappings": MAPPING_FOR_INDEX,
                "settings": {
                    "index.default_pipeline": pipeline_id,
                },
            },
        )
        logging.info(response)
    except BadRequestError:
        logging.info(f'Index "{INDEX_NAME}" already exists')
    finally:
        await elastic_client.close()