from elasticsearch import AsyncElasticsearch
import config
from aiokafka import AIOKafkaConsumer
import json
import logging
import sys
import asyncio
from script_for_index_creation import create_index


logging.basicConfig(format='%(asctime)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    stream=sys.stdout)

logger = logging.getLogger()
logger.setLevel(logging.INFO)


elastic_client = AsyncElasticsearch(hosts=config.ELASTICSEARCH_HOST)


async def consume_and_store_data():
    consumer = AIOKafkaConsumer(
        config.CONSUMER_TOPIC,
        bootstrap_servers=[config.BOOTSTRAP_SERVER],
        auto_offset_reset="earliest",
        enable_auto_commit=True,
        value_deserializer=lambda x: json.loads(x.decode("utf-8")),
        key_deserializer=lambda x: x.decode("utf-8"),
    )

    await consumer.start()

    try:
        async for msg in consumer:
            logging.debug(f"Consumed msg with key: {msg.key} to be stored in elastic")
            document_id = msg.value['server'] + '-' + str(msg.value['id'])
            result = await elastic_client.index(index='discounts', id=document_id, document=msg.value)
            if result.meta.status not in [200, 201]:
                logging.error(f"Document with id {document_id} has not been saved to Elasticsearch: {result}")
            logging.debug(f"Document with id {document_id} saved to Elasticsearch: {result}")

    finally:
        await consumer.stop()
        await elastic_client.close()


if __name__ == "__main__":
    asyncio.run(create_index())
    asyncio.run(consume_and_store_data())
