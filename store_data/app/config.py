import os

ELASTICSEARCH_HOST = os.getenv('ELASTICSEARCH_HOST', 'http://localhost:9200')
BOOTSTRAP_SERVER = os.getenv('BOOTSTRAP_SERVER', 'localhost:29092')
CONSUMER_TOPIC = os.getenv('CONSUMER_TOPIC', 'cleaned-discounts')