from aiokafka import AIOKafkaProducer
import asyncio
import logging
import config
import sys
from api_fetchers import RohlikFetcherApp, KosikFetcherApp


logging.basicConfig(format='%(asctime)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    stream=sys.stdout)

logger = logging.getLogger()
logger.setLevel(logging.INFO)


async def main():
    producer = AIOKafkaProducer(bootstrap_servers=config.KAFKA_BROKER)
    await producer.start()

    kosik_fetcher = KosikFetcherApp(producer)
    rohlik_fetcher = RohlikFetcherApp(producer)

    try:
        while True:
            # Fetch data from both KosikFetcherApp and RohlikFetcherApp
            kosik_task = asyncio.create_task(kosik_fetcher.fetch_data_from_api())
            rohlik_task = asyncio.create_task(rohlik_fetcher.fetch_data_from_api())
            await asyncio.gather(kosik_task, rohlik_task)

            await asyncio.sleep(30 * 60)  # Sleep for 30 minutes
    finally:
        await producer.stop()


if __name__ == "__main__":
    asyncio.run(main())

