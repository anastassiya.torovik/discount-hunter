from abc import ABC, abstractmethod
import asyncio
from httpx import AsyncClient
import aiometer
from aiokafka import AIOKafkaProducer
import json
import config


class ApiException(Exception):
    def __init__(self, message):
        self.message = message


class ApiFetcherApp(ABC):

    @abstractmethod
    async def fetch_data_from_api(self, *args, **kwargs) -> None:
        """The method fetches data of discounted products from external APIs """
        pass


class KosikFetcherApp(ApiFetcherApp):
    KOSIK_URL = 'https://www.kosik.cz/api/front/page/products?slug=akce'

    def __init__(self, producer: AIOKafkaProducer):
        self.producer = producer

    async def fetch_data_from_api(self) -> None:
        async with AsyncClient() as session:
            # First, retrieve the total count of items
            response = await session.get(KosikFetcherApp.KOSIK_URL)
            if not response:
                raise ApiException(f'Unable to get a valid response from Kosik.'
                                   f' Status: {response.status_code}, message {response.text}')
            try:
                count = response.json()['products']['totalCount']
            except KeyError:
                raise ApiException(f'Unable to get a count from Kosik response: {response.json()}')

            # Loop through and retrieve data in batches of 30 items
            batch_size = 30
            for offset in range(0, count, batch_size):
                response = await session.get(KosikFetcherApp.KOSIK_URL + f'&limit={batch_size}&offset={offset}')
                if not response:
                    raise ApiException(f'Unable to get a valid response from Kosik.'
                                       f' Status: {response.status_code}, message {response.text}')
                data = response.json()

                items = data['products']['items']
                for item in items:
                    await self.produce_message(item)

    async def produce_message(self, item: dict) -> None:
        key = str(item['id']).encode('utf-8')
        payload = json.dumps(item).encode('utf-8')
        await self.producer.send_and_wait(config.KOSIK_TOPIC, payload, key=key)


class RohlikFetcherApp(ApiFetcherApp):
    CATEGORIES = [
        300121429, 300101000, 300102000, 300103000, 300104000, 300105000, 300107000,
        300106000, 300108000, 300112393, 300109000, 300111000, 300110000, 300112000, 300112909
    ]

    def __init__(self, producer: AIOKafkaProducer):
        self.producer = producer
        self.session = AsyncClient()

    async def fetch_data_from_api(self) -> None:
        tasks = [self._fetch_category_products(category) for category in RohlikFetcherApp.CATEGORIES]
        await asyncio.gather(*tasks)

    async def _fetch_product_info(self, product_id: int):
        ROHLIK_URL_PRODUCT = f"https://www.rohlik.cz/api/v1/products?products={product_id}"
        ROHLIK_URL_PRICE = f"https://www.rohlik.cz/api/v1/products/{product_id}/prices"

        response_product = await self.session.get(ROHLIK_URL_PRODUCT, timeout=20)
        response_product.raise_for_status()
        product_data = response_product.json()[0]

        response_price = await self.session.get(ROHLIK_URL_PRICE, timeout=20)
        response_price.raise_for_status()
        price_data = response_price.json()

        item = {**product_data, **price_data}
        await self.produce_message(item)

    async def produce_message(self, item: dict) -> None:
        key = str(item.get('id')).encode('utf-8')
        payload = json.dumps(item).encode('utf-8')
        await self.producer.send_and_wait(config.ROHLIK_TOPIC, payload, key=key)

    async def _fetch_category_products(self, category: int) -> None:
        ROHLIK_URL_PRODUCTS = f"https://www.rohlik.cz/api/v1/categories/sales/{category}/products?page=0&size=1000"
        response = await self.session.get(ROHLIK_URL_PRODUCTS, timeout=20)
        response.raise_for_status()
        data = response.json()
        try:
            product_ids = data["productIds"]
        except KeyError:
            raise ApiException(f'Unable to get discounted product ids from Rohlik response: {data}')

        await aiometer.run_on_each(
            self._fetch_product_info,
            product_ids,
            max_per_second=3
        )