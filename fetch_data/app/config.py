import os
KAFKA_BROKER = os.getenv('KAFKA_BROKER', 'localhost:29092')
KOSIK_TOPIC = os.getenv('KAFKA_TOPIC', 'kosik-source-discounts')
ROHLIK_TOPIC = os.getenv('ROHLIK_TOPIC', 'rohlik-source-discounts')
